#!/usr/bin/env python3

# Copyright 2018-2024 CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file LICENCE.md.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organisation or
# submit itself to any jurisdiction.

"""Perform a query in the Rucio database.

This utility aims to be an alternative to software like SQL*Plus.  What it
lacks in features, it makes up for in convenience.  It leverages SQLAlchemy and
cx_Oracle and, thus, requires an environment where the Rucio core can be used.
Though designed with SELECT statements primarily in mind, others like UPDATE
and DELETE should also be possible.
"""

import argparse
import os
import subprocess
import sys
from collections.abc import Iterable, Sequence
from datetime import datetime
from typing import Optional

from sqlalchemy import text
from sqlalchemy.orm import Session
from tabulate import tabulate

from rucio.db.sqla.session import get_session, read_session

CSV_SEPARATOR = ','
PAGER = 'less'
PAGER_OPTIONS = '-FRSXMK'
TABULATE_FORMAT = 'presto'


def main(argv: Sequence) -> Optional[str]:
    """The effective entry point of this script."""
    args = parse_args(argv)
    if args.file:
        with open(args.query) as f:
            query = f.read()
    else:
        query = args.query

    if args.explain:
        return explain(query)
    else:
        return execute(query, args)


def parse_args(argv: Sequence) -> argparse.Namespace:
    """Parse the command-line arguments."""
    parser = argparse.ArgumentParser(description=__doc__)
    output = parser.add_mutually_exclusive_group()
    parser.add_argument('query',
                        help='the query to perform',
                        metavar='QUERY')
    output.add_argument('--csv',
                        help='output the results as comma-separated values',
                        action='store_true')
    parser.add_argument('--explain',
                        help='display the execution plan chosen by the Oracle optimizer',
                        action='store_true')
    parser.add_argument('--file',
                        help='handle QUERY as a file and read its content',
                        action='store_true')
    parser.add_argument('--no-header',
                        help='omit the header with the column labels',
                        action='store_false',
                        dest='header')
    output.add_argument('--vertical',
                        help='output the results one column per line',
                        action='store_true')
    return parser.parse_args(argv)


@read_session
def explain(query: str, *, session: Session) -> Optional[str]:
    """Display the execution plan chosen by the Oracle optimizer."""
    try:
        session.execute(text(f'EXPLAIN PLAN FOR {query}'))
        plan = session.execute(text('SELECT * FROM TABLE(dbms_xplan.display())'))
    except Exception as error:
        return str(error)

    display_with_pager('\n'.join(plan.scalars().all()))


def execute(query: str, args: argparse.Namespace) -> Optional[str]:
    """Perform a query in the Rucio database."""
    session = get_session()
    start = datetime.now()

    try:
        results = session.execute(text(query))
    except Exception as error:
        return str(error)

    finish = datetime.now()
    duration = round((finish - start).total_seconds(), ndigits=1)

    if results.returns_rows:
        if args.csv:
            if args.header:
                print(*results.keys(), sep=CSV_SEPARATOR)
            for row in results:
                print(*rawtohex(row), sep=CSV_SEPARATOR)
        elif args.vertical:
            labels = tuple(results.keys())
            width = max(map(len, labels))
            for count, row in enumerate(results, start=1):
                if count > 1:
                    print('-' * width)
                for index, column in enumerate(rawtohex(row)):
                    print(f'{labels[index]:>{width}}', ':', column)
        else:
            display_with_pager(tabulate(tabular_data=(rawtohex(row) for row in results),
                                        headers=results.keys() if args.header else (),
                                        tablefmt=TABULATE_FORMAT))
        print(f'Fetched {results.rowcount} rows in {duration} seconds.', file=sys.stderr)
    else:
        print(f'Matched {results.rowcount} rows in {duration} seconds.')
        answer = input('Commit the changes to the database? (Type uppercase yes): ')
        if answer == 'YES':
            session.commit()
            print('Committed the transaction.')
        else:
            session.rollback()
            print('Rolled back the transaction.')


def display_with_pager(content: str) -> None:
    """Display some content with the use of an external pager."""
    with subprocess.Popen(args=(PAGER, PAGER_OPTIONS),
                          stdin=subprocess.PIPE,
                          universal_newlines=True) as pager:
        pager.communicate(content)


def rawtohex(row: Iterable) -> tuple:
    """Implicitly convert binary columns to strings.

    This avoids the necessity to use RAWTOHEX() in the query for RAW columns
    containing UUIDs.
    """
    return tuple(map(lambda c: c.hex().upper() if isinstance(c, bytes) else c, row))


if __name__ == '__main__':
    try:
        sys.exit(main(sys.argv[1:]))
    except KeyboardInterrupt:
        print()
    except BrokenPipeError:
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
