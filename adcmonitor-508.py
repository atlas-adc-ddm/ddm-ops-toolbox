#!/usr/bin/env python3

################################################################
# As per https://its.cern.ch/jira/browse/ADCMONITOR-508.       #
#                                                              #
# KISS, please: https://en.wikipedia.org/wiki/KISS_principle   #
################################################################

import smtplib
import socket
import subprocess
import sys

from datetime import datetime, timedelta
from email.message import EmailMessage
from enum import Enum
from pathlib import Path

from rucio.client.ruleclient import RuleClient

# akin to rucio.db.sqla.constants
class RuleState(Enum):
  REPLICATING = 'R'
  STUCK = 'S'
  SUSPENDED = 'U'

class KnownError(Enum):
  a = 'Target RSE set not sufficient for number of copies'
  b = 'There is insufficient quota on any of the target RSEs to fulfil the operation'
  c = 'MissingSourceReplica'
  d = 'RSE excluded due to write blacklisting'
  # z = 'RequestErrMsg.TRANSFER_FAILED:Failed to stat file (No such file or directory)'


def summaryR(rules):
  
  for hours in [4, 12, 24, 36]:
    older_than_hours = [ x for x in rules if (datetime.utcnow() - x.get('created_at')).total_seconds() > (hours*3600) ]
    fsummary.write(f'- older than {hours} hours: {len(older_than_hours)}\n')
  
  return


def summaryS(rules, top=3):
  
  count_unknown_errors = len(rules)
  for e in KnownError:
    x = [ x for x in rules if str(x.get('error')) in e.value ]
    fsummary.write(f'{len(x)} : {e.value}\n')
    count_unknown_errors -= len(x)
  fsummary.write(f'{count_unknown_errors} : Some other problem\n')
  
  fsummary.write(f'Top {top} by STUCK locks count (STATE[OK/REPL/STUCK]):\n')
  if len(rules) > 0:
    sorted_rules=sorted(rules, key=lambda d: d['locks_stuck_cnt'], reverse=True)
    for i in sorted_rules[0:top]:
      fsummary.write(f"[{i['locks_ok_cnt']}/{i['locks_replicating_cnt']}/{i['locks_stuck_cnt']}] {i['rse_expression']} {i['id']} {str(i['scope'])}:{i['name']}\n")
  else:
    fsummary.write(f'- None\n')
  
  return


def summaryU(rules):
  
  count_unknown_errors = len(rules)
  for e in KnownError:
    x = [ x for x in rules if str(x.get('error')) in e.value ]
    fsummary.write(f'{len(x)} : {e.value}\n')
    count_unknown_errors -= len(x)
  fsummary.write(f'{count_unknown_errors} : Some other problem\n')
  
  return


def report(account,rule_state):
  
  freport.write(f'# begin report for: account={account}, rule_state={rule_state}\n')
  
  rules = [ x for x in myruleclient.list_replication_rules({'account':account, 'state':rule_state}) if x.get('activity') == 'T0 Export' ]
  for r in rules:
    freport.write(f'{r}\n')
  
  fsummary.write(f'\n# {RuleState(rule_state).name} rules for {account}: {len(rules)}\n')
  if rule_state == 'R':
    summaryR(rules)
  elif rule_state == 'S':
    summaryS(rules)
  elif rule_state == 'U':
    summaryU(rules)
  
  freport.write('# end report for: ' + 'account=' + account + ', rule_state=' + rule_state + '\n')
  
  return len(rules) 


################################################################
#                            START                             #
################################################################

#### Init

r2d2_url = 'https://rucio-ui.cern.ch/r2d2?account=tzero&activity=T0%20Export&state=REPLICATING&interval=2d'

stoday = datetime.today().strftime("%Y%m%d")
local_basedir = f'/tmp/adcmonitor-508/{stoday}'
export_basedir = f'root://eosuser.cern.ch//eos/user/d/ddmusr01/shared/adcmonitor-508/{stoday}'
export_url = f'https://cernbox.cern.ch/index.php/apps/files/?dir=/__myshares/shared%20(id:486347)/adcmonitor-508/{stoday}'

report_fullpath = f'{local_basedir}/report_{stoday}'
summary_fullpath = f'{local_basedir}/summary_{stoday}'


#### Get the relevant rules and dump the results

Path(local_basedir).mkdir(parents=True, exist_ok=True)

fsummary = open(summary_fullpath, "w")

fsummary.write(f'Robotic report of the [REPL/STUCK/SUSP] T0 Export RULES for tzero/ddmadmin.\n')
fsummary.write(f'''A full report can be found in
  (xrootd) <{export_basedir}>,
  (https)  <{export_url}>.\n''')
fsummary.write(f'Generated on {datetime.today().strftime("%d-%B-%Y %H:%M")} from {socket.getfqdn()}. Responsibles: Fabio Luchetti <fabio.luchetti@cern.ch>.\n')


myruleclient=RuleClient()

freport = open(report_fullpath, "w")

# tzero
len_tzeroR=report('tzero','R')
len_tzeroS=report('tzero','S')
len_tzeroU=report('tzero','U')

# ddmadmin
len_ddmadminR=report('ddmadmin','R')
len_ddmadminS=report('ddmadmin','S')
len_ddmadminU=report('ddmadmin','U')


freport.close()

fsummary.write(f'\nFor more info and query customisation please consult R2D2, e.g., <{r2d2_url}>.\n')
fsummary.close()


#### Forward the summary as email

msg = EmailMessage()

msg['Subject'] = f'[REPL/STUCK/SUSP] T0 Export RULES by account: tzero[{len_tzeroR}/{len_tzeroS}/{len_tzeroU}] , ddmadmin[{len_ddmadminR}/{len_ddmadminS}/{len_ddmadminU}]'
msg['From'] = 'ddmusr01@cern.ch'
# msg['To'] = 'faluchet@cern.ch'
msg['To'] = 'atlas-adc-ddm-support@cern.ch'
with open(summary_fullpath) as fp:
  msg.set_content(fp.read())

s = smtplib.SMTP()
s.connect()
s.send_message(msg)
s.quit()


#### Archive the report

# cmd = f'gfal-mkdir --parents {export_basedir}'
cmd = f'gfal-copy --parent --force {local_basedir} {export_basedir}'
cmd = cmd.split()

p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout, stderr = p.communicate()
