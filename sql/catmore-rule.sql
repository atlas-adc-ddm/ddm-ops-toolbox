/* Copyright 2018-2023 CERN
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3), copied verbatim in the file LICENCE.md.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organisation
 * or submit itself to any jurisdiction. */

/* List rules eligible for deletion by the Catmore Rule.
 *
 * The Catmore Rule is a space-saving procedure that allows identifying and
 * deleting the replication rules of datasets on disk that have not been
 * recently used and have a copy on tape.  It is an operational procedure
 * typically applied once per month. */

/* First, select the replication rules that satisfy most conditions, without
 * yet taking into account the last access or criteria applied to specific data
 * formats. */
WITH catmore_rule AS (
    SELECT
        RAWTOHEX(rules_1.id) AS rule,
        rules_1.rse_expression,
        dids.datatype,
        dids.bytes,
        dataset_locks_1.accessed_at,
        dataset_locks_1.created_at,
        dataset_locks_1.scope,
        dataset_locks_1.name
    FROM atlas_rucio.dataset_locks dataset_locks_1
    JOIN atlas_rucio.rules rules_1
    ON dataset_locks_1.rule_id = rules_1.id
    JOIN atlas_rucio.dids
    ON
        dataset_locks_1.scope = dids.scope
        AND dataset_locks_1.name = dids.name
    WHERE
        /* Select rules on official datasets. */
        (dataset_locks_1.scope LIKE 'data%'
            OR dataset_locks_1.scope LIKE 'mc%'
            OR dataset_locks_1.scope LIKE 'valid%')
        /* Replicated on a DATADISK. */
        AND dataset_locks_1.rse_id IN (
            SELECT rse_id
            FROM atlas_rucio.rse_attr_map
            WHERE key = 'type' AND value = 'DATADISK'
        )
        /* Created by DDM or PanDA. */
        AND rules_1.account IN ('ddmadmin', 'panda')
        /* That are not locked. */
        AND rules_1.locked = 0
        /* That are not in the process of being moved elsewhere. */
        AND rules_1.child_rule_id IS NULL
        /* Limited to rules on datasets. */
        AND rules_1.did_type = 'D'
        /* Having at least one complete and permanent replica on an appropriate
         * tape RSE. */
        AND EXISTS (
            SELECT 1
            FROM atlas_rucio.dataset_locks dataset_locks_2
            JOIN atlas_rucio.rules rules_2
            ON dataset_locks_2.rule_id = rules_2.id
            WHERE
                dataset_locks_2.scope = dataset_locks_1.scope
                AND dataset_locks_2.name = dataset_locks_1.name
                AND dataset_locks_2.rse_id IN (
                    SELECT rse_id
                    FROM atlas_rucio.rse_attr_map
                    WHERE
                        key = 'type' AND value IN ('MCTAPE', 'DATATAPE')
                        OR key = 'CERN-PROD_DERIVED'
                )
                AND rules_2.state = 'O'
                AND rules_2.expires_at IS NULL
        )
)
/* Then, limit the selection to datasets that have a last access outside the
 * defined time window.  By default, this is six months.  However, it was
 * agreed by CREM that it may reduced to two months for AODs and HITS.  EVNTs
 * should be completely excluded from the procedure. */
SELECT *
FROM catmore_rule
WHERE
    (COALESCE(accessed_at, created_at) < SYSDATE - 180
        OR datatype = 'AOD' AND COALESCE(accessed_at, created_at) < SYSDATE - 60
        OR datatype = 'HITS' AND COALESCE(accessed_at, created_at) < SYSDATE - 60)
    AND datatype <> 'EVNT'
ORDER BY COALESCE(accessed_at, created_at)
