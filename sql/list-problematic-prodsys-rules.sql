/* Copyright 2022-2023 CERN
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3), copied verbatim in the file LICENCE.md.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organisation
 * or submit itself to any jurisdiction. */

/* List non-OK rules created by ProdSys on RSEs that are no longer nuclei.
 *
 * For staging data from tape, ProdSys is creating replication rules with the
 * RSE expression 'type=DATADISK&datapolicynucleus=True'.  If the RSEs chosen
 * at the time the locks were created are demoted, the affected rules might
 * become problematic (in case the become STUCK, the Judge Repairer will be
 * unable to handle them).  There is an issue related to this:
 *     https://github.com/rucio/rucio/issues/536
 *
 * Since problematic rules cannot be fixed neither automatically by Rucio nor
 * manually by DDM Ops, they should be identified and reported to ProdSys. */

SELECT
    RAWTOHEX(rules.id) AS rule,
    rules.state,
    rules.expires_at,
    rules.locks_ok_cnt || '/'
        || rules.locks_replicating_cnt || '/'
        || rules.locks_stuck_cnt AS locks,
    atlas_rucio.id2rse(dataset_locks.rse_id) AS rse,
    rules.scope,
    rules.name
FROM atlas_rucio.rules
JOIN atlas_rucio.dataset_locks
ON rules.id = dataset_locks.rule_id
WHERE
    rules.account = 'prodsys'
    AND rules.activity = 'Staging'
    AND rules.rse_expression = 'type=DATADISK&datapolicynucleus=True'
    AND rules.state <> 'O'
    AND dataset_locks.rse_id NOT IN (
        SELECT rse_id
        FROM atlas_rucio.rse_attr_map
        WHERE key = 'datapolicynucleus' AND value = 'true'
    )
ORDER BY rules.expires_at
